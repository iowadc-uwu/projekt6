from __future__ import annotations

import glob
import os
import time

import supersuit as ss
import numpy as np
from stable_baselines3 import PPO
from stable_baselines3.ppo import CnnPolicy, MlpPolicy

from pettingzoo.butterfly import knights_archers_zombies_v10
from pettingzoo.mpe import simple_spread_v3

import matplotlib.pyplot as plt

from stable_baselines3.common.callbacks import BaseCallback


class RewardLoggerCallback(BaseCallback):
    """
    A custom callback that logs the reward at the end of each episode.
    """
    def __init__(self, model_name, verbose: int = 0):
        super().__init__(verbose)
        self.episode_rewards = []
        self.current_rewards = 0
        self.model_name = model_name

    def _on_step(self) -> bool:
        # Accumulate rewards for the current episode
        self.current_rewards += np.sum(self.locals['rewards'])
        return True

    def _on_rollout_end(self) -> None:
        # Log the accumulated rewards at the end of the episode
        self.episode_rewards.append(self.current_rewards)
        self.current_rewards = 0

    def _on_training_end(self) -> None:
        # Plot the rewards at the end of training
        plt.figure(figsize=(10, 6))
        plt.plot(self.episode_rewards, label='Episode Reward')
        plt.xlabel('Episode')
        plt.ylabel('Reward')
        plt.title('Episode Rewards Over Time')
        plt.legend()
        plt.grid(True)
        os.makedirs("projekt6/plots", exist_ok=True)
        plt.savefig(f"projekt6/plots/{self.model_name}_episode_rewards_over_time.png")
        plt.show()


def train(env_fn, model_name, hyperparams, steps: int = 10_000, seed: int | None = 0, **env_kwargs):
    # Train a single model to play as each agent in an AEC environment
    env = env_fn.parallel_env(**env_kwargs)

    # Add black death wrapper so the number of agents stays constant
    # MarkovVectorEnv does not support environments with varying numbers of active agents unless black_death is set to True
    env = ss.black_death_v3(env)

    env.reset(seed=seed)

    print(f"Starting training on {str(env.metadata['name'])}.")

    env = ss.pettingzoo_env_to_vec_env_v1(env)
    env = ss.concat_vec_envs_v1(env, 8, num_cpus=1, base_class="stable_baselines3")

    model = PPO(
        MlpPolicy,
        env,
        verbose=3,
        batch_size=hyperparams['batch_size'],
        n_steps=hyperparams['n_steps'],
        learning_rate=hyperparams['learning_rate'],
        gamma=hyperparams['gamma'],
        clip_range=hyperparams['clip_range'],
        ent_coef=hyperparams['ent_coef']
    )

    reward_logger = RewardLoggerCallback(model_name=model_name)

    model.learn(total_timesteps=steps, callback=reward_logger)

    os.makedirs("projekt6/models", exist_ok=True)
    model.save(f"projekt6/models/{model_name}_{time.strftime('%Y%m%d-%H%M%S')}")

    print("Model has been saved.")

    print(f"Finished training on {str(env.unwrapped.metadata['name'])}.")

    env.close()


def eval(env_fn, num_games: int = 100, render_mode: str | None = None, **env_kwargs):
    env = env_fn.env(render_mode=render_mode, **env_kwargs)

    print(
        f"\nStarting evaluation on {str(env.metadata['name'])} (num_games={num_games}, render_mode={render_mode})"
    )

    try:
        latest_policy = max(
            glob.glob(f"{env.metadata['name']}*.zip"), key=os.path.getctime
        )
    except ValueError:
        print("Policy not found.")
        exit(0)

    model = PPO.load(latest_policy)

    rewards = {agent: 0 for agent in env.possible_agents}

    for i in range(num_games):
        env.reset(seed=i)
        env.action_space(env.possible_agents[0]).seed(i)

        for agent in env.agent_iter():
            obs, reward, termination, truncation, info = env.last()

            for a in env.agents:
                rewards[a] += env.rewards[a]

            if termination or truncation:
                break
            else:
                if agent == env.possible_agents[0]:
                    act = env.action_space(agent).sample()
                else:
                    act = model.predict(obs, deterministic=True)[0]
            env.step(act)
    env.close()

    avg_reward = sum(rewards.values()) / len(rewards.values())
    avg_reward_per_agent = {
        agent: rewards[agent] / num_games for agent in env.possible_agents
    }
    print(f"Avg reward: {avg_reward}")
    print("Avg reward per agent, per game: ", avg_reward_per_agent)
    print("Full rewards: ", rewards)
    return avg_reward

def load_and_eval(env_fn, model_path, num_games=10, render_mode=None, **env_kwargs):
    model = PPO.load(model_path)
    eval(env_fn, num_games=num_games, render_mode=render_mode, **env_kwargs)


hyperparams_set_1 = {
    'learning_rate': 3e-4,
    'n_steps': 2048,
    'batch_size': 64,
    'gamma': 0.98,
    'clip_range': 0.2,
    'ent_coef': 0.01
}

hyperparams_set_2 = {
    'learning_rate': 1e-3,
    'n_steps': 4096,
    'batch_size': 128,
    'gamma': 0.98,
    'clip_range': 0.2,
    'ent_coef': 0.02
}

hyperparams_set_3 = {
    'learning_rate': 1e-4,
    'n_steps': 1024,
    'batch_size': 32,
    'gamma': 0.98,
    'clip_range': 0.1,
    'ent_coef': 0.005
}


if __name__ == "__main__":
    env_fn = simple_spread_v3
    env_kwargs = dict(N=3, local_ratio=0.5, max_cycles=25)

    train(env_fn, model_name="model3", hyperparams=hyperparams_set_3, steps=500_000, seed=0, **env_kwargs)

    # eval(env_fn, num_games=10, render_mode=None, **env_kwargs)
    # eval(env_fn, num_games=2, render_mode="human", **env_kwargs)

